# Docker - TP Final

## Datos de cursada

### Nombre: Josue Andres Lopez Saldias

### Fecha: 07/12/21

### Materia: Infraestructura de Servidores

## Visualizador de infraestructura
El visualizador de infraestructura es una aplicacion que permite visualizar algunos diagramas basicos de infraestructura de **AWS** usando las librerias boto3 y diagrams. La aplicacion es una maqueta hecha para el trabajo practico final de la materia Infraestructura de Servidores de ISTEA.

### Instalacion
#### Obtencion
Para obtener la aplicacion hay dos maneras:
---
> __NOTA__: Requiere tener instalado docker. Si no lo tiene instalado puede consultar [ACA](https://docs.docker.com/engine/install/) 
Requiere tambien tener cuenta de AWS para ejectarlo 


1. Docker pull. Ejecutando el siguiente comando podremos obtener la imagen desde el repo de gitlab
    ```bash
    docker pull registry.gitlab.com/jeyseven/istea-infra-servidores/tp-final:latest
    ```
2. Git Clone.  
    a. Hacemos git clone usando el siguiente comando
    ```bash
    git clone https://gitlab.com/jeyseven/istea-infra-servidores.git 
    ```
    b. Nos movemos al directorio donde esta nuestro Dockerfile
    ```bash
    cd ./istea-infra-servidores/tp-final
    ``` 
    c. Luego ejecutamos **docker build** para crear la imagen
    ```bash
    docker build -t registry.gitlab.com/jeyseven/istea-infra-servidores/tp-final:latest .
    ```
    d. Para comprobar si se genero la imagen podemos correr
    ```bash
    docker image ls
    ```
    Deberiamos poder ver nuestra imagen con el nombre registry.gitlab.com/jeyseven/istea-infra-servidores/tp-final:latest 
  
#### Ejecucion
1. Vamos a correr el comando **docker run** de la siguiente manera:
```bash
## Reemplazar ${ACA_VA_TU_AWS_ACCESS_KEY_ID},${ACA_VA_TU_AWS_SECRET_ACCESS_KEY} y ${ACA_VA_TU_AWS_SESSION_TOKEN}
## Por el valor correspondiente
docker run -d -p 8080:5000 -e AWS_ACCESS_KEY_ID=${ACA_VA_TU_AWS_ACCESS_KEY_ID} -e AWS_SECRET_ACCESS_KEY=${ACA_VA_TU_AWS_SECRET_ACCESS_KEY} -e AWS_SESSION_TOKEN=${ACA_VA_TU_AWS_SESSION_TOKEN} --name visualizador  registry.gitlab.com/jeyseven/istea-infra-servidores/tp-final:latest
```
2. Verificamos si el contenedor esta corriendo usando:
```bash
docker ps
```
3. Vamos nuestro navegador y ponemos http://localhost:8080


### Como usar?
1. Seleccionamos nuestra region
| ![Lista de regiones](img/region.png) |
|:--:|
|  *Imagen de la lista de regiones* |
2. Seleccionamos los diagramas que queremos generar
| ![Diagramas](img/diagramas.png) |
|:--:|
|  *Imagen de diagramas* |

## Dockerfile
El Dockerfile esta compuesto de las siguientes lineas:
1. Bajamos la imagen de python version 3.9.9 de DockerHub. Esta sera nuestra imagen base

```Dockerfile
FROM python:3.9.9-alpine
```

2. Configuramos LABELS para agregar informacion relacionada a la imagen que vamos a crear

```Dockerfile
LABEL mainteiner="Josue Lopez"
LABEL version="alpha"
LABEL description="TP FINAL"
```

3. Descargamos graphviz y ttf-freefont ya que es lo que permite generar las imagenes png. Es una dependencia de la libreria Diagrams
```Dockerfile
RUN apk add --update --no-cache \
       graphviz \
       ttf-freefont
```

4. Copiamos el archivo requirements.txt (el archivo contiene las dependencias de nuestra aplicacion) en el directorio /app dentro de la imagen

```Dockerfile
COPY requirements.txt /app/requirements.txt
```

5. Indicamos nuestro espacio de trabajo con WORKDIR y corremos la instalacion de las librerias que nuestra aplicacion requiere.

```Dockerfile
WORKDIR /app
RUN pip install -r requirements.txt
```

6. Indicamos un Volumen dentro del dockerfile para que no sea necesario indicar un volumen a la hora de hacer docker run. 
Al indicarlo dentro del Dockerfile impedimos que haya una bajada de performance en la aplicacion en el caso de que se olvide montar un volumen.
El volumen tiene ese directorio ya que es donde se guardaran los png generados 
```Dockerfile
VOLUME ./static/png
```

7. Indicamos nuestra aplicacion para que se ejecute al momento de iniciar nuestro contenedor

```Dockerfile
ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
```
## Subida imagen del contenedor a repositorio
La subida al repositorio se hace usando gitlabci

## Librerias

[Diagrams](https://github.com/mingrammer/diagrams)

[Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)
