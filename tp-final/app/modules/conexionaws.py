import boto3
import os

awsConnect = boto3.Session(aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
                            aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'],
                            aws_session_token=os.environ['AWS_SESSION_TOKEN'])

def get_regions():
    ec2Client = awsConnect.client('ec2',region_name='us-west-2')
    response = ec2Client.describe_regions()
    regions = [region['RegionName'] for region in response['Regions']]
    return regions

def get_elbv2(region):
    elbv2Client = awsConnect.client('elbv2',region_name=region)
    elbs = [{elbv2['LoadBalancerName']:elbv2['LoadBalancerArn']} for elbv2 in elbv2Client.describe_load_balancers()['LoadBalancers']]
        
    elbResource = {}
    for i in elbs:
        targetGroups = {}
        for targetGroupArn in elbv2Client.describe_target_groups(LoadBalancerArn=''.join(i.values()))['TargetGroups']:
            targetGroups[targetGroupArn['TargetGroupName']] = [tgInstances['Target']['Id'] for tgInstances in elbv2Client.describe_target_health(TargetGroupArn=targetGroupArn['TargetGroupArn'])['TargetHealthDescriptions']]
        elbResource[''.join(i.keys())] = targetGroups
    
    return elbResource
    
def get_cloudfront(region):
    cloudfrontClient = awsConnect.client('cloudfront',region_name=region)
    distributions = {}
    for distributionId in cloudfrontClient.list_distributions()['DistributionList']['Items']:
        distributions[distributionId['Id']] = [originsDomainName['DomainName'] for originsDomainName in distributionId['Origins']['Items']]
    
    return distributions
    
def get_vpc(region):
    ec2Resources = awsConnect.resource('ec2',region_name=region)
    ec2Client = awsConnect.client('ec2',region_name=region)

    vpcsId = {}
    for vpc in ec2Client.describe_vpcs()['Vpcs']:
        for vpcTags in vpc['Tags']:
            if vpcTags['Key'] == "Name":
                vpcsId[vpc['VpcId']] = f"{vpcTags['Value']} - Cidr: {vpc['CidrBlock']}"
                break
        if 'Name' not in vpcTags.values():
            vpcsId[vpc['VpcId']] = f"Sin Nombre - Cidr: {vpc['CidrBlock']}"
                
    
    resultado = {}
    for vpcId in vpcsId.keys():
        subnetEc2Instancias = {}
        for subnet in ec2Client.describe_subnets(Filters=[{'Name': 'vpc-id','Values':[vpcId]}])['Subnets']:
            for subnetTags in subnet['Tags']:
                if subnetTags['Key'] == "Name":
                    title = f"{subnetTags['Value']} - Cidr: {subnet['CidrBlock']}" 
            if 'Name' not in vpcTags.values():
                title = f"Sin Nombre - Cidr: {subnet['CidrBlock']}"
                
            subnetEc2Instancias[title] = [instance.id for instance in ec2Resources.Subnet(subnet['SubnetId']).instances.all()]
        
        
        resultado[vpcsId[vpcId]] = subnetEc2Instancias
    
        
    return resultado