from diagrams import Diagram, Cluster
from diagrams.aws.compute import EC2,ECS
from diagrams.generic.blank import Blank
from diagrams.aws.network import CloudFront, ALB, APIGateway
from diagrams.aws.storage import S3
import re

def diagram_cloudfront(data:dict):
    n=0
    for i,j in data.items():
        with Diagram(f"Diagrama Cloudfront: {i}", filename=f"static/png/cloudfront/cloudfront{n}", show= False,outformat='png'):
            lista = []
            for x in j:
                if re.match(".*elb.*",x):
                  lista.append(ALB(x.split('.')[0][:25]))
                elif re.match(".*s3.*",x):
                  lista.append(S3(x.split('.')[0][:25]))
                elif re.match(".*api.*",x):
                  lista.append(APIGateway(x.split('.')[0][:25]))
            CloudFront(i) >> lista
        n+=1
    
def diagram_vpc(data:dict):
    n=0
    for i,j in data.items():
        with Diagram(f"Instancias EC2 - VPC: {i}", filename=f"static/png/ixvpc/vpc{n}", show= False,outformat='png', direction="TB"):
            with Cluster(i):
                for x,y in j.items():
                    lista = []
                    with Cluster(x):
                        if not y:
                            lista.append(Blank("Nada conectado"))
                        for z in y:
                            lista.append(EC2(z))
        n+=1

def diagram_elb(data:dict):
    n = 0
    for i,j in data.items():
        with Diagram(f"Diagrama ELB: {i}", filename=f"static/png/elb/elb{n}", show= False,outformat='png'):
            for x,y in j.items():
                lista = []
                with Cluster(x):
                    if not y:
                        lista.append(Blank("Nada conectado"))
                    for z in y:
                        if re.match("^\d{1,3}[.]\d{1,3}[.]\d{1,3}[.]\d{1,3}",z):
                            lista.append(ECS(z))
                        elif re.match("^i-.*",z):
                            lista.append(EC2(z))       
            ALB(i) >> lista
        n+=1