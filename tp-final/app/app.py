from flask import Flask, render_template
from modules import diagramas, conexionaws
import os

app = Flask(__name__)


regions = conexionaws.get_regions()

@app.route("/")
def index():
    data={
        'titulo': 'Tp Final',
        'h1': 'Visualizador de infraestructura',
        'listregion': regions
    }
    return render_template("index.html", data=data)

@app.route("/<selectedRegion>")
def menu(selectedRegion):
    if selectedRegion in regions:
        data={
            'titulo': selectedRegion,
            'h1': 'Seleccione una opcion',
            'region': selectedRegion
        }
        return render_template("index.html", data=data)
    return render_template('error.html'),404

@app.route("/<selectedRegion>/<selectedDiagram>")
def opcion_elegida(selectedRegion,selectedDiagram):
    options = ['cloudfront','elb','ixvpc']
    if selectedRegion in regions:
        if selectedDiagram in options:
            if selectedDiagram == 'cloudfront':
                diagramas.diagram_cloudfront(conexionaws.get_cloudfront(selectedRegion))
                lista = os.listdir('static/png/cloudfront')
            elif selectedDiagram == 'elb':
                diagramas.diagram_elb(conexionaws.get_elbv2(selectedRegion))
                lista = os.listdir('static/png/elb')
            elif selectedDiagram == 'ixvpc':
                diagramas.diagram_vpc(conexionaws.get_vpc(selectedRegion))
                lista = os.listdir('static/png/ixvpc')
            data={
                'titulo': selectedDiagram,
                'h1': 'Diagramas Generados',
                'files': lista,
                'region': selectedRegion
            }
            return render_template("index.html", data=data)
    return render_template('error.html'),404


if __name__== "__main__":
    app.run(host='0.0.0.0')