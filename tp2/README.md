# Cloud Computing

## Datos de cursada

### Nombre: Josue Andres Lopez Saldias

### Fecha: 05/10/21

### Materia: Infraestructura de Servidores

## Definicion

Teniendo en cuenta definiciones vistas en [GCP](https://cloud.google.com/learn/what-is-cloud-computing?hl=es), [IBM](https://www.ibm.com/es-es/cloud/learn/cloud-computing-gbl), [RedHat](https://www.redhat.com/es/topics/cloud) y [Azure](https://azure.microsoft.com/es-es/overview/what-is-cloud-computing/), podemos decir que el cloud computing es el nombre comercial con el que el mercado denomina al suministro de servicios informaticos (como servidores, redes, software y bases de datos) a traves de internet. Al ser un servicio ofrecido por un proveedor, evita que las empresas tengan que encargarse de aprovisionar, configurar o gestionar distintos recursos, reduciendo costos, ademas de que solo se pague por lo que usen.

### Que diferencia hay entre cloud compunting y un servicio de alquiler de datacenter o un servicio de hosting tradicional?
La mayor diferencia es la multilocacion, pudiendo decidir donde queremos que nuestros servicios y datos sean alojados.

### Que ventajas trae esto para las empresas?
* Permite a las pequeñas empresas acceder a tecnologia avanzada: Si tenemos en cuenta el modelo on premise, la pequeñas empresas solo podian acceder a modestos servidores debido al costo de cada equipo. Con el cloud computing las pequeñas empresas acceden a la misma tecnologia que las grandes empresas pagando lo mismo que estas.
* Costo: Se reduce la inversion de capital al no necesitar adquirir hardware y software, y la configuracion, refirgeracion asi como tambien los expertos que mantendran el datacenter. Con el cloud computing solo se solicita lo que se necesita y se paga por su uso. 
* Escala global: Permite escalar cuanto se necesite, y en distintas partes del mundo.
* Seguridad: Los proveedores al tener varios clientes, suelen tener una inversion mucho mayor en seguridad que lo que se podria tener on-premise. Tambien ofrecen herramientas que permiten administrar la seguridad en capas superiores ya dependientes del cliente.
* Velocidad: Permite que se pueda solicitar un servicio o recurso a demanda, teniendo por ejemplo: un servidor de 64 CPU y 256GB de ram en tan solo minutos
* Productividad: Al no requerir administracion de servidores fisicos por parte de una empresa, permite que sus administradores puedan dedicar toda su atencion a la administracion de los sistemas que necesite la compañia  
* Confiabilidad: Permite crear copias de seguridad con facilidad, asi como tambien crear replicas en varias regiones (datacenters) evitando downtime prolongado.

## Caracteristicas Generales

Segun el NIST estas son las caracteristicas de cloud computing:

* On-demand self-service (Autoservicio bajo demanda): Permite adquirir recursos de manera automatica y lo que se necesite sin la intervencion del proveedor.

* Broad network access (Accesible a traves de toda la red): Todos los recursos deben estar disponible a traves de la red, ya sea red privada o publica.

* Resource pooling (Agrupacion de recursos): Esto quiere decir que los recursos del proveedor estan agrupados como uno solo, para satisfacer la necesidad de varios clientes. Esto no quiere decir que un cliente accede a la informacion de otro, sino que ambos pueden estar ocupando un mismo hardware pero a nivel de datos estan aislados.

* Rapid elasticity or expansion (Rapida elasticidad o expansion): Si bien la escalabilidad, es decir crecer en infraestructura, es tambien posible en cloud computing, hay un termino mas especifico y que es caracteristico, este es la elasticidad. Rapida elasticidad hace referencia a la velocidad con nuestra infraestructura puede crecer y decrecer segun se requiera.

* Measured service (Servicio Medido): El uso de cada recurso es medido y observado, dando una buena visibilidad de todo lo que se usa. Se puede observar datos de transferencias, lectura y escritura de disco, uso de CPU, cantidad de request, etc.

Si bien no es mencionado por NIST, tambien podes agregar:
* Pago por uso: Solo se paga por lo que se usa, si necesitamos una instancia con 2 CPU y 8GB RAM, mas 200GB de almacenamiento, solo se pagara por eso que se solicito, ni mas ni menos.

## Modelo de capas
El modelo de capas hace referencia a los diferentes modelos de servicio que ofrecen los proveedores de cloud computing. Estos se adaptan a las necesidades que tengan los clientes ya sean usuarios o empresas, proporcionando un nivel diferente de control, seguridad y escalabilidad. Vale aclarar que una empresa puede hacer uso de los tres modelos en simultaneo

Nos encontramos con tres capas o modelo de servicios:

### Saas (Software as a service o Software como servicio)
El Saas (software como servicio) es un modelo de servicio de cloud computing que permite a los usuarios acceder a un software administrado por un proveedor. Los usuarios no instalan aplicaciones en sus pc o smartphones, sino que las aplicaciones se acceden a traves de una red remota mediante la web o una API. Al ser una aplicacion no instalable facilita la velocidad y el trabajo colaborativo en proyectos.

#### Características principales:

* Modelo de subscripcion: El servicio se proporciona mediante un modelo de subscripcion, permitiendo que solo se pague por la cantidad de cuentas y/o modulos habilitados, por ejemplo.
* El software es gestionado por el proveedor: Esto facilita el mantenimiento de la aplicacion por el usuario, por ejemplo instalacion de acutalizaciones.
* Informacion resguardada: Al ser software de acceso remoto, permite que ante una falla en el dispositivo local, no se pierdan datos ya que toda la informacion es almacenada remotamente.
* Escalamiento: Si una empresa necesita mas recursos es facilmente eslable, sin la necesidad de comprar mas hardware por parte del cliente.
* Acceso desde cualquier parte: Al no ser instalables permite que se puedan acceder desde una pc, smartphone, tablet, etc., sin mayores dificultades ademas como depende de internet permite ser accedidos desde casi cualquier parte del mundo.

### Paas (Platform as a service o Plataforma como servicio)
El Paas (plataforma como servicio) es otro modelo de servicio de cloud computing que permite a los usuarios acceder a un entorno de desarrollo e implementación completo en la nube. Incluye infraestructura como servidores, almacenamiento y redes, en un nivel mas abstracto que lo que se puede encontrar en Iaas, es decir, no vamos a tener la maquina virtual para administrar, solo podemos indicar cuanto recurso necesitamos para nuestra aplicacion, y el proveedor se encarga de mantener las Instancias. Este modelo también incluye middleware, herramientas de desarrollo, servicios de BI, sistemas de administración de bases de datos, etc. Paas está diseñado para facilitar el despliegue de una aplicacion de cualquier programador sin la necesidad de tener grandes conocimientos de infraestructura.

#### Características principales:

* Entorno de deasarrollo: Suele incluir herramientas para probar, desarrollar y desplegar aplicaciones todo en un mismo entorno.
* Orientacion por el desarrollo: Permite que los desarrolladores solo se focalicen en el desarrollo, sin necesitar tener grandes conocimientos a nivel de infraestructura. Esto ayuda muchisimo para microemprendimientos de software.
* El proveedor es el sysadmin: Todo lo referido a seguridad, acutalizaciones de sistema operativo, instalacion de dependencias, revision de vulnerabilidades a nivel capa servidor, copias de seguridad son gestionadas por el proveedor.
* Facilita la colaboración: Al ser un modelo de servicio cloud permite que distintos equipos puedan trabajar en conjunto, sin la necesidad de estar en un mismo sitio.

### Iaas (Infrastructure as a service o Infraestructura como servicio)
La Iaas (infraestructura como servicio) es el tercer modelo de servicio de cloud computing que permite a los usuarios acceder a los recursos informáticos mas orientadas a infraestructura como almacenamiento, red y servidores. Ofrece una gran flexibilidad para escalar y reducir verticalmente los recursos cuando necesite, asi como tambien permite el escalamiento horizontal en el caso que necesite nuevas instancias para su aplicacion.
Iaas evita parte del costo de mantener, configurar, y comprar nuevo hardware para un Data Center, asi como tambien permite que los sysadmin se puedan enfocar a administrar los middleware y recursos que requiera una aplicacion. Al ser un modelo de servicio cloud cada recurso se ofrece como un componente de servicio aparte, y permite solo pagar por lo que se necesite de algun recurso en concreto.

#### Características principales:

* No mas servidores fisicos: Permite que en lugar de comprar nuevo hardware fisico, los usuarios adquieran los recursos necesarios a demanda.
* Escalabilidad horizontal y vertical (elasticidad): Permite escalar de manera facil y rapida, ya sea alamacenamiento y procesamiento.
* Ahorra costos: Ayuda a que las empresas ahorren el coste de comprar y mantener su propio hardware, asi como tambien permite que las pequeñas empresas puedan acceder a hardware potente.
* Alta disponibilidad: Al ser hardware remoto, permite que haya gran resilencia, evitando perdida de datos por problemas de hardware.
* Productividad: Al liberar a los sysadmin del tedioso trabajo de mantenimiento de hardware, permite que estos se puedan enfocar en otras tareas que el negocio considere necesarias.

| ![Imagen de modelos de servicio](https://www.stackscale.com/wp-content/uploads/2020/04/modelos-servicios-cloud-iaas-paas-saas-stackscale.jpg) |
|:--:|
|  *Imagen de modelos de servicio by stackcale* |

### Proveedores

#### [AWS](https://aws.amazon.com/es/what-is-aws/)
Es una de las mas grandes proveedores de cloud computing, tiene presencia en 190 países alrededor del mundo y es uno de los proveedores mas usados a nivel mundial. Mayormente orientado a Iaas, podemos encontrar tambien algunos servicios en los otros modelos:

##### Algunos servicios IaaS
* EC2: Servicio para crear instancias (Maquinas virtuales)
* EBS: Servicio para almacenamiento por bloque
* VPC: Servicio de redes
* EKS: Servicio de Kubernetes. En modo no fargate se crean instancias que pueden ser administradas por un administrador

##### Algunos servicios PaaS
* EKS (fargate): El modo fargate, AWS administra los nodos asi como tambien el Control Plane
* DynamoDB: Servicio de bases no relacionales, la administracion es hecha por AWS
* RDS: Servicio de bases relacionales, la administracion es hecha por AWS
* SQS: Servicio de brocker, administrado por AWS

##### Algunos servicios SaaS
* La interfaz de administracion de AWS
* CloudWatch
* OpenSearch

#### [GCP](https://cloud.google.com/)
Es otra de los grandes proveedores. Al igual que AWS esta mas orientado a Iaas, sin embargo podemos mencionar algunos servicios que corresponde a los demas modelos

##### IaaS
* GCE: Servicio de instancias
* Persistent Disk: Servicio de almacenamiento por bloques
* GKE: Servicio de Kubernetes, GCP administra el control plane
* VPC: Servicio de redes

##### PaaS
* Cloud SQL: Servicio de bases relacionales administrado por GCP
* Firestore: Servicio de bases no relacionales
* CloudRun: Servicio para levantar contenedores
* AppEngine: Servicio Paas por excelencia de GCP.

##### SaaS
* Cloud Monitoring
* Cloud Trace
* Google Maps
* Google Workspace

#### Proveedores meramente Paas
Estos proveedores solo estan enfocados en Paas. Si bien los anteriores tambien podrian ser considerados Paas ya que tienen varios servicios enfocados al desarrollo, los siguientes estan enfocados a esto en su totalidad
##### [Heroku](https://www.heroku.com/)
Citado de la pagina de Heroku: *Heroku es una plataforma clodu que permite a las compañias crear, entregar, monitorear y escalar aplicaciones. Son una forma rapida de llevar una idea a una URL, evitando los dolores de cabeza de una infraestructura*

##### [OpenShift](https://www.redhat.com/es/technologies/cloud-computing/openshift)
Citado de la pagina de RedHat:  *es la plataforma de Kubernetes empresarial líder en el sector, la cual se diseñó especialmente para formar parte de una estrategia de nube híbrida abierta. Gracias a que ofrece operaciones automatizadas integrales, una experiencia uniforme en todos los entornos y la implementación de autoservicio para los desarrolladores, los equipos pueden trabajar en conjunto para llevar las ideas de la etapa de desarrollo a la de producción de manera más eficiente*

##### [CloudFoundry](https://www.cloudfoundry.org/)
Citado de cloudacademy.com: *Cloud Foundry is an open source cloud computing platform originally developed in-house at VMware. It is now owned by Pivotal Software, which is a joint venture made up of VMware,  EMC, and General Electric*. Permite empaquetar un codigo y poder deployar en varios proveedores de servicio como AWS, GCP, Azure, etc.

#### Proveedores meramente Saas
Para Saas podemos encontrar muchisimos, ya que hoy en dia la mayoria de aplicaciones se proveen usando el metodo Saas
Ejemplos:
* Netflix
* Atlassian
    * Jira
    * Confluence
* Adobe
    * Adobe creative cloud

## Ejemplos de capas
### Iaas
* Levantar una instancia con mysql o otro motor de base de datos: Seria casi como crear una Maquina Virtual con un mysql, el servicio de mysql lo administrariamos nosotros y el proveedor solo nos daria, las vpc, subnets, storage y procesamiento. Vale aclarar que la administracion del SO tambien pasaria a ser nuestra responsabilidad.
* Levantar una instancia bastion: Al igual que el ejemplo anterior todo lo que se encuentre dentro de la instancia seria nuestra responsabilidad, el proveedor solo brindara poder de procesamiento, seguridad (aunque en este caso lo que es a nivel de FW sera nuestra responsabilidad), storage y red.

### Paas
* Levantar una aplicacion usando AppEngine en GCP: El servicio de AppEngine nos provee todo, solo necesitamos seleccionar que lenguaje y version necesitamos para nuestra creacion, subir nuestro codigo a la plataforma y nuestra aplicacion ya estara arriba. Si bien se puede decidir optimizar los recursos de procesamiento, no pasaria a ser Iaas, ya que no tenemos acceso a la infraestructura donde esta levantada nuestra aplicacion.

### Saas
* Brindar una aplicacion que se acceda por internet: Seria una aplicacion, API o midleware que se accesible por internet, ejemplo un webmail (como roundcube o zimbra).

## Fuentes

[Final Version of NIST Cloud Computing Definition Published](https://www.nist.gov/news-events/news/2011/10/final-version-nist-cloud-computing-definition-published)

[Servicios de GCP](https://cloud.google.com/products?hl=es)

[What is cloudfoundry?](https://cloudacademy.com/blog/cloud-foundry-benefits/)

[What is Heroku?](https://www.heroku.com/what)

[Que es redhat openshift?](https://www.redhat.com/es/technologies/cloud-computing/openshift)