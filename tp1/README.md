# Instalación puppet server

## Datos de cursada

### Nombre: Josue Andres Lopez Saldias

### Fecha: 14/09/21

### Materia: Infraestructura de Servidores

## Descripción

La idea es levantar un servidor primario de puppet para luego. El servidor contara con una particion exclusivamente para uso del servicio puppet.
Se comparten dos maneras para realizar la configuracion:
* Vagrant: Permite crear una segunda instancia para testear.
* Manual: Solo se da la guia para levantar el master. Sin embargo con los scripts que estan en el [repositorio](https://gitlab.com/jeyseven/istea-infra-servidores/-/tree/main/tp1), se podra configurar una vm para el agente.

## Requisitos

| Name | Source | Version |
|------|--------|---------|
| vagrant | hashicorp | >= 2.2.18 |
| virtualbox | oracle | >= 6.1 |

### VM Base

#### Vagrant
Se crearan dos instancias una para puppet-master y otra para el agente, ambas tendran las siguientes carateristicas
Box: ubuntu/bionic64
CPU: 2
Memoria: 1GB
Disco: 40GB

Solo el puppet-master
Disco Secundario: 10GB

#### Manual
Esta sera la imagen base donde se instalara el SO:
Memoria: 1GB **(NOTA: El servicio puppet requiere 2GB minimo en un ambiente productivo [Link](https://puppet.com/docs/puppet/6/server/install_from_packages.html#install-puppet-server))**
Disco: 12GB
Imagen: Ubuntu 18.04.05 (64 bit) suministrada por el profesor [LINK DE DESCARGA](https://drive.google.com/file/d/1szhLQ31YMd7wc4FC2yqrH0HZvodDLeB-/view?usp=sharing)

### Disco extra

Se montara un disco para el uso de puppet, el tamaño sera:
Disco: 10 GB

## Instrucciones

### Vagrant

* Primero hacer git clone del repo
`git clone https://gitlab.com/jeyseven/istea-infra-servidores.git`

* Nos movemos a la carpeta 
`cd ./istea-infra-servidores/tp1/`

#### Comandos
Crear las VM

```bash
## Dentro del directorio donde esta el archivo Vangrantfile
vagrant up
```

Conectarse por ssh

```bash
## puppet-master
vagrant ssh puppetprimary
## puppet-agent
vagrant ssh puppetagent
```

Ver status

```bash
vagrant status
```

### Manual

* Primero debemos crear una maquina virtual con la imagen suministrada por el profesor [LINK DE DESCARGA](https://drive.google.com/file/d/1szhLQ31YMd7wc4FC2yqrH0HZvodDLeB-/view?usp=sharing)

* Adaptador de red puente

* Un disco secundario 10GB

#### Configuración basica de la VM

1. Setear el hostname. Teniendo en cuenta que será el servidor puppet master, le vamos a llamar **puppet-master**. Esto lo setearemos con el siguiente comando.
   `hostnamectl set-hostname puppet-master`

2. Configuraremos una ip fija en nuestro servidor. Para esto editaremos el archivo **/etc/netplan/01-netcfg.yaml**
   1. Primero verificar cual es la ip que tiene asignada por dhcp en la vm
   `ip a`

   2. Verificamos la direccion de Gateway
   `ip route | grep default | awk '{print $3}'`

   3. A partir de la ip conseguida anteriormente podemos seleccionar una IP fija para nuestra VM

   ```yaml
   ## This file describes the network interfaces available on your system
   ## For more information, see netplan(5).
   network:
     version: 2
     renderer: networkd
     ethernets:
       enp0s3:
         dhcp4: no
         addresses: [{_IP-SELECCIONADA_}/24]
         gateway4: {_IP-GATEWAY_}
         nameservers:
           addresses: [8.8.8.8,8.8.4.4]
   ```

   4. Aplicamos los cambios
   `sudo netplan apply`

3. Vamos a configurar el firewall a nuestro servidor. Para esto vamos a seguir lo siguiente:
   1. Se denegara todo el trafico entrante por default. Esto permitirá que solo se le de acceso a lo que necesitamos posteriormente. Sin embargo el trafico saliente se habilitara por completo

   __NOTA: Asegurarse que el servicio este inactivo, sino cortara el acceso por ssh. `sudo ufw status`__.

   ``` bash
   ### Deshabilitamos el trafico entrante
   sudo ufw default deny incoming
   ### Habilitamos el trafico saliente
   sudo ufw default allow outgoing
   ```

   2. Se permitirá solo el acceso a los siguientes puertos.
      * 22 por el ssh usado para conectarse al servidor y por el code manager para el git clone por ssh
      * 443 y 80 para descargar configuraciones de internet
      * 8140 es el puerto que sirve de comunicación entre el servidor primario y los agentes de puppet. Se usa para verificar estados
      * 8142 puerto que también es usado en la comunicación entre el servidor primario y los agentes. Se usa para la ejecución delos agentes
      * 8170 lo usa el code manager para desplegar entornos, correr webhooks y crear llamadas API.

    ``` bash
    ### Habilitamos el trafico entrante de ssh
    sudo ufw allow ssh
    ### Habilitamos el trafico entrante de http
    sudo ufw allow http
    ### Habilitamos el trafico entrante de https
    sudo ufw allow https
    ### Habilitamos el trafico entrante de 8140
    sudo ufw allow 8140
    ### Habilitamos el trafico entrante de 8142
    sudo ufw allow 8142
    ### Habilitamos el trafico entrante de 8170
    sudo ufw allow 8170
    ```

   3. Por ultimo habilitamos el ufw
   `sudo ufw enable`

4. Montaje del disco creado para uso exclusivo de puppet
   1. Primero vamos a instalar lvm
   `sudo apt update && sudo apt -y install lvm2`
   2. Verificamos cual es el nombre de nuestro segundo disco
   `lsblk`
   En mi caso es el disco sdb
   3. Procedemos a crear nuestro lvm
      1. Creamos nuestro volumen fisico
      `sudo pvcreate /dev/sdb`
      2. Creamos nuestro grupo de volumenes
      `sudo vgcreate puppet /dev/sdb`
      3. Creamos un volumen lógico. En este caso solo le daremos 5GB de los 10GB que tiene el disco. También le vamos a poner de nombre **puppet-master**
      `sudo lvcreate puppet -L 5G -n puppet-master`
   4. Aplicamos un filesystem al volumen creado anteriormente
   `sudo mkfs.ext4 /dev/puppet/puppet-master`
   5. Crearemos el directorio /opt/puppetlab/
   `sudo mkdir -p /opt/puppetlabs`
   6. Montamos nuestro lvm en el directorio creado anteriormente
   `sudo mount /dev/puppet/puppet-master /opt/puppetlabs`
   7. Agregamos nuestro montaje al fstab
   `echo "UUID=$(lsblk -f | grep puppet | awk {'print $3'}) /opt/puppetlab ext4 defaults 0 0" | sudo tee -a /etc/fstab`

5. Configuración preliminar
Antes de iniciar con la instalacion de puppet, es necesario configurar el archivo **/ect/hosts**. Esto nos ayudara a que se resuelva el nombre puppet-master con la IP de nuestro servidor
`echo "$(hostname -I)  puppet-master" | sudo tee -a /etc/hosts`

6. Instalación de puppet
   1. Vamos a descargar el paquete que nos permitira instalar el repositorio de puppet
   `wget -O repositorio-puppet7.deb https://apt.puppetlabs.com/puppet7-release-$(cat /etc/os-release | grep VERSION_CODENAME | cut -d= -f2).deb`

   2. Una vez descargado, necesitamos agregar el repositorio
   `sudo dpkg -i repositorio-puppet7.deb`
   Y actualizamos nuestra lista de repositorios
   `sudo apt update`

   3. Luego procedemos a instalar puppet
   `sudo apt install -y puppetserver`

7. Configuración del servicio
   1. Debido a que se levanto una VM con 1GB memoria, vamos modificar la cantidad de memoria que usara la JVM de puppet. En este caso le vamos a dejar solo 512m.
   Para esto vamos a editar el archivo **/etc/default/puppetserver**

   ```bash
   sudo sed -i 's|JAVA_ARGS="-Xms2g -Xmx2g -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger|JAVA_ARGS="-Xms512m -Xmx512m -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger|g' /etc/default/puppetserver
   ```

   2. Vamos a configurar puppet para que el codedir vaya al disco montado en el paso 4.
      1. Primero vamos a crear los diretorios y vamos cambiar los permisos:
         * **/opt/puppetlabs/code**

      ``` bash
      sudo mkdir -p /opt/puppetlabs/code
      
      ```

      2. Luego vamos a configurar nuestro puppet.conf con la siguiente configuracion

      ``` cnf
      # This file can be used to override the default puppet settings.
      # See the following links for more details on what settings are available:
      # - https://puppet.com/docs/puppet/latest config_important_settings.html
      # - https://puppet.com/docs/puppet/latest/config_about_settings.html
      # - https://puppet.com/docs/puppet/latest/config_file_main.html     
      # - https://puppet.com/docs/puppet/latest/configuration.html
      
      [master]
      vardir = /opt/puppetlabs/server/data/puppetserver
      rundir = /var/run/puppetlabs/puppetserver
      pidfile = /var/run/puppetlabs/puppetserver/puppetserver.pid     
      codedir = /opt/puppetlabs/code
      dns_alt_names = puppet-master

      [main]
      certname = puppet-master
      server = puppet-master
      environment = production
      runinterval = 15m

      ## This is only for testing
      [server]
      autosign = true
      ```

      Para editarlo hacemos:

      ``` bash
      ## Instalamos vim
      sudo apt install -y vim 
      ## Editamos el archivo de configuracion
      sudo vim /etc/puppetlabs/puppet/puppet.conf
      ```

      3. Y editamos el archivo **/etc/puppetlabs/puppetserver/conf.d/puppetserver.conf**
      ``` bash
      ## Modificamos donde estara la carpeta code
      sudo sed -i 's|server-code-dir: /etc/puppetlabs/code|server-code-dir: /opt/puppetlabs/code|g' /etc/puppetlabs/puppetserver/conf.d/puppetserver.conf
      ```

      4. Movemos los archivos de /etc/puppetlabs/code a /opt/puppetlabs/code
      `sudo mv /etc/puppetlabs/code /opt/puppetlabs/code`

      **OPCIONAL EN CASO DE QUERER HACER UN TEST**
      5. Añadimos un archivo para hacer test del agente
         1. Creamos el directorio
         `sudo mkdir -p /opt/puppetlabs/code/environments/production/manifests/`
         2. Creamos el archivo **/opt/puppetlabs/code/environments/production/manifests/site.pp** con el siguiente codigo 

         ```pp
         node /default/ {

             file { "/home/HolaMundo": # Recurso tipo file 

                     ensure => "directory", # crea el directorio

                     owner => "root", # Owner

                     group => "root", # grupo

                     mode => "0755", # permisos de diretorio
                  }
         }
         ```

   3. Creamos la firma que usara el servidor de puppet
   `sudo /opt/puppetlabs/bin/puppetserver ca setup`

8. Levantamos el servicio de puppet y lo habilitamos en el inicio
   `sudo systemctl start puppetserver && sudo systemctl enable puppetserver`

9. Por ultimo comprobamos el inicio con 
`systemctl status puppetserver`

## Comentario

En el caso de querer levantar una VM para testear el agente. Recordar agregar la IP del servidor puppet-master en el /etc/hosts de la vm-agente.
`echo "{_IP-PUPPET-MASTER_}  puppet-master" | sudo tee -a /etc/hosts`

## Fuentes

[Installing Puppet Server](https://puppet.com/docs/puppet/6/server/install_from_packages.html)

[Puertos que deben estar abiertos](https://puppet.com/docs/pe/2019.8/system_configuration.html#firewall_standard)

[Disable Autosign](https://puppet.com/docs/puppet/6/ssl_autosign.html#ssl_disable_autosigning)